<?php

    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');

    $hewan = new Animal("Shaun");
    echo "Name : " . $hewan->name; // "shaun"
    echo "<br>";
    echo "Legs : " . $hewan->legs; // 4
    echo "<br>";
    echo "Cold Blooded : " . $hewan->cold_blooded;
    echo "<br><br>";
    
    $kodok = new Frog("buduk");
    echo "Name : " . $kodok->name; // "shaun"
    echo "<br>";
    echo "Legs : " . $kodok->legs; // 4
    echo "<br>";
    echo "Cold Blooded : " . $hewan->cold_blooded;
    echo "<br>";
    echo "Jump : " . $kodok->jump();
    echo "<br><br>";

    $sungokong = new Ape("Kera Sakti");
    echo "Name : " . $sungokong->name; // "shaun"
    echo "<br>";
    echo "Legs : " . $sungokong->legs; // 4
    echo "<br>";
    echo "Cold Blooded : " . $sungokong->cold_blooded;
    echo "<br>";
    echo "Yell : " . $sungokong->yell();
    echo "<br><br>";
?>
